# m1if20

## IP

Premier : 192.168.1.93

192.168.1.131

192.168.1.115



## Lancer le serveur

Ajustez les paramètres dans config/server_config.py (l'adresse IP est celle de la machine sur laquelle
le serveur est lancé)

Exécutez server.py depuis la machine qui héberge le serveur

```bash
$ python3 ia/server.py
```

Envoyez le contenu voulu dans un robot en lançant la commande :

```bash
chmod +x ./cp_robot.sh
./cp_robot.sh ipaddr dirOrFile1 [dirOrFile2, [..]]
```

## Connexion ssh au robot
* se connecter au réseau Oasis-TD8 (clef : @Deay2dh&)
* connecter le robot au même réseau wifi
* $ ssh robot@[adresse ip du robot] (mot de passe : maker)

Sur le robot, lancez le main de [robot_client/client.py](robot_client/README.md) :

```bash
cd robot_client/
python3 client.py
```

Vous devez voir sur votre machine des informations sur l'état de la connexion.  


# Serveur Discord  

https://discord.gg/JK2jD7c

# Lien Rapport de projet

https://docs.google.com/document/d/1mGiXga-y60EvZThbeJB5ZSfcvfoEQ7OfDJCuSADGCSs/edit?usp=sharing
