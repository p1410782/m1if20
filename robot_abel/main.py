# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import time

import ev3dev
import ev3dev.ev3 as ev3
from robot import *
from laby import *

if __name__ == "__main__":
    RUN_STATE = False

    # 1 : pos X du robot
    # 2 : pos Y du robot
    # 3 : taille en X du laby
    # 4 : taille en Y du laby
    # 5? : orientation
    robot = Robot(0,0,5,5,"E")
    laby = robot.getLaby()


    #### exemple de declaration de Laby
    # laby = Laby(5,5,2,2)
    #### exemple de declaration de Laby

    # look left and right and output stats
    # robot.stats_right_left()

    #######################################################
    #################### PHASE DE SCAN ####################
    #######################################################

    print("My current Laby : ")
    laby.printLaby()

    robot.ilFautSuivreLeConduit(16)

    print("My NEW Laby after scanning : ")
    laby = robot.getLaby()
    laby.printLaby()


    ###########################################################
    #################### FIN PHASE DE SCAN ####################
    ###########################################################

    # avancer d'une case
    #robot.avancer()
    # tourner droite
    # robot.tournerCouleur()
    # tourner gauche 
    #robot.tourner(-1)
    # demi-tour
    #robot.tourner(2.25)
    # reculer d'une case
    #robot.avancer()

    #robot.forward_until_obstacle()
    # robot.forward_left_obstacle()

    #demi tour !
    #robot.turnBack_obstacle()


    # look left
    # left = robot.regarder_gauche()
    # print('distance left :' , left)

    # look right
    # right = robot.regarder_droite()
    # print('distance right :' , right)

    # whereis Panel
    # left, right = robot.regarder()
    # distanceL = robot.panel_dist(left)
    # distanceR = robot.panel_dist(right)
    # print('distance left CM :' , left)
    # print('distance right CM :' , right)
    # print('distance left :' , distanceL)
    # print('distance right :' , distanceR)

    # WIGGLE WIGGLE WIGGLE
    #robot.wiggle()
    # robot.wiggleIfOtherBot()

    # suivre la ligne blanche
    # robot.ilFautSuivreLeConduit()
