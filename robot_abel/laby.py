# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import time

#Classe pour tester chez soi le client/serveur
class Laby:

	def __init__(self, largeur, longueur, posX, posY):
		self.largeur = largeur
		self.longueur = longueur
		self.nbMurs = ((largeur-1)*largeur) + ((longueur-1)*longueur)
		self.matriceMurs = [1 for i in range(self.nbMurs)]
		self.matriceCases = [0 for i in range(longueur*largeur)]
		# (posX,posY) : position du robot
		self.posRobot = posX+posY*largeur
		self.posXRobot = posX
		self.posYRobot = posY
		self.matriceCases[self.posRobot] = 1

	# casse le mur a la position (x,y)
	# le try catch est la pour eviter que l'application plante
	def breakMur(self, index):
		try:
			self.matriceMurs[index] = 0
			pass
		except Exception:
			print("breakMur : Bad arguments")

	# a partir de l'ancienne position (oldX,oldY) du robot
	# definit la nouvelle position dans le labyrinthe a (newPosX,newPosY)
	def setPosRobot(self, oldX, oldY, newPosX, newPosY):
		try:
			self.posXRobot = newPosX
			self.posYRobot = newPosY
			self.matriceCases[self.posRobot] = 0
			self.posRobot = newPosX+newPosY*self.largeur
			self.matriceCases[self.posRobot] = 1
			pass
		except Exception:
			print("setPosRobot : Bad arguments")
	
	def getLargeur(self):
		return self.largeur

	def getLongueur(self):
		return self.longueur

	def getPosRobot(self):
		return self.posRobot
	
	def getPosXrobot(self):
		return self.posXRobot

	def getPosYrobot(self):
		return self.posYRobot

	def findMurToBreak(self, orientation, posX, posY):
		if orientation == "N" and posY > 0 :
			murNord = posY*(self.largeur-1)+(self.largeur*(posY-1))+posX
			return murNord
		elif orientation == "S" and posY < self.longueur-1:
			murSud = (posY+1)*(self.largeur-1)+(self.largeur*posY)+posX
			return murSud
		elif orientation == "E" and posX < self.largeur-1:
			murEst = posY*(self.largeur-1)+(self.largeur*posY)+posX
			return murEst
		elif orientation == "O" and posX > 0:
			murOuest = posY*(self.largeur-1)+(self.largeur*posY)+posX-1
			return murOuest
		else:
			return None

	# return True : si le mur existe en face du robot
	def murExiste(self, orientation, posX, posY):
		indexMur = self.findMurToBreak(orientation, posX, posY)
		if indexMur is not None:
			if self.matriceMurs[indexMur] == 1:
				return True
		return False

	def breakMurNord(self, posX, posY):
		self.breakMur(self.findMurToBreak("N", posX, posY))

	def breakMurSud(self, posX, posY):
		self.breakMur(self.findMurToBreak("S", posX, posY))

	def breakMurEst(self, posX, posY):
		self.breakMur(self.findMurToBreak("E", posX, posY))

	def breakMurOuest(self, posX, posY):
		self.breakMur(self.findMurToBreak("O", posX, posY))

	# orientation : orientation de la tete du robot
	# posX : position en X du robot
	# posY : position en Y du robot
	# nbMur : nombre de murs a casser
	# on fait gaffe de verifier que le nombre de mur a casser est coherent
	# si je suis en X == 2 et orientation == Ouest et que je tente de casser 3 mur, ca ne marchera pas
	def multipleMurBreak(self, orientation, posX, posY, nbMur=1):
		if orientation == "N" and posY-nbMur+1 > 0 :
			for i in range(nbMur):
				self.breakMurNord(posX, posY-i)
		elif orientation == "S" and posY+nbMur < self.longueur:
			for i in range(nbMur):
				self.breakMurSud(posX, posY+i)
		elif orientation == "E" and posX+nbMur < self.largeur:
			for i in range(nbMur):
				self.breakMurEst(posX+i, posY)
		elif orientation == "O" and posX-nbMur+1 > 0:
			for i in range(nbMur):
				self.breakMurOuest(posX-i, posY)


	def printLaby(self):
		# print('\n'.join([''.join(['{:4}'.format(item) for item in row]) for row in self.matriceMurs]))
		nbElemsLargeur = ((self.largeur*2)+1)
		nbElemsLongueur = ((self.longueur*2)+1)
		indexMur = 0
		indexCase = 0

		self.printFullWall(nbElemsLargeur)

		nbElemsLongueur -= 2
		for j in range(nbElemsLongueur):
			tmpStr = ""
			# cases et murs
			if j%2 == 0:
				nbMurs = self.largeur-1
				tmpStr += "|"
				for i in range(nbMurs):
					if self.matriceCases[indexCase] == 1:
						tmpStr += " R "
					else:
						tmpStr += "   "
					if self.matriceMurs[indexMur] == 1:
						tmpStr += "|"
					else:
						tmpStr += " "
					indexMur += 1
					indexCase += 1
				if self.matriceCases[indexCase] == 1:
					tmpStr += " R |"
				else:
					tmpStr += "   |"
				print(tmpStr)
			# only murs
			else:
				nbMurs = self.largeur
				tmpStr += "+"
				for i in range(nbMurs):
					if self.matriceMurs[indexMur] == 1:
						tmpStr += "---"
					else:
						tmpStr += "   "
					tmpStr += "+"
					indexMur += 1
				indexCase += 1
				print(tmpStr)

		self.printFullWall(nbElemsLargeur)

	def printFullWall(self, nbElems):
		tmpStr = ""
		for item in range(nbElems):
			if item%2 == 0:
				tmpStr += "+"
			else:
				tmpStr += "---"
		print(tmpStr)