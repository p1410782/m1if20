#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time

#Classe pour tester chez soi le client/serveur
class Robot:

	def __init__(self):
		self.client = None

	def avancerJusquaObstacle(self):
		print("Je suis un robot qui avance")
		time.sleep(1)
		print("J'avance")
		time.sleep(1)
		print("Encore!")
		print("J'ai touch� un mur!")
		time.sleep(.5)
		print("Je le signale..")
		self.sendMessage("ObstacleEnAvancant")

	def sendMessage(self, msg):
		if self.client is not None:
			self.client.send(msg)

	def setClient(self, client):
		self.client = client