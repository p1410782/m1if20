#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import socket
import pickle
import sys
import json
import threading

#from explore import *

from robot import *

#sys.path.append("..")
# Changer avec le bon robot :
#from robot4roues.robotTest import Robot
#from robot_eve.robot import Robot
from server_config import SERVER_ADDRESS, PORT

class RobotClient:
	def __init__(self):
		print("Init robot client")
		self.robot = Robot()
		self.robot.setClient(self)
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		print("Try to connect to... " + str(SERVER_ADDRESS) + " on port = " + str(PORT))
		self.socket.connect((SERVER_ADDRESS, PORT))
		self.connected = True
		print("Connected to " + str(SERVER_ADDRESS))

	def run(self):
		while self.connected:
			print("Attente d'un message....")
			data = self.socket.recv(1024) 
			if data == b'':
				self.connected = False  
			if data :
				self.processMessage(pickle.loads(data))
			#cmd = data.decode("utf-8") #Expect message
			#self.processMessage(cmd)
		print("Exiting...")
		self.socket.close()

	def processMessage(self,msg):
		""" Les messages reçus par le serveur
			Le serveur envoie une direction (E,O,N,S) et le robot réagit en fonction
		"""
		#print("ProcessMessage " + msg)
		if isinstance(msg, tuple) :
			#Le tuple signifie qu'on envoie la taille du labyrinthe
			# (labX, labY)
			self.robot.setLabSize(*msg) # * == unpack
		elif isinstance(msg, list) :
			#La liste signifie qu'on envoie la position initiale du robot
			# [X,Y,orientation]
			self.robot.setInitialPos(*msg)
		elif isinstance(msg, str) :
			#Messages d'actions (avancer ou découverte maze..)
			if msg == "DISCOVER":
				self.discoverMaze()
			else :
				if msg == "E":
					self.robot.avancerEst()
				elif msg == "S":
					self.robot.avancerSud()
				elif msg == "W":
					self.robot.avancerOuest()
				elif msg == "N":
					self.robot.avancerNord()
				#Envoie OK au serveur
				self.send("OK")

	def send(self, message):
		"""Envoi d'un message au serveur
		"""
		try:
			stringMsg = pickle.dumps(message)
			# self.socket.send(message.encode())
			self.socket.send(stringMsg)
		except:
			self.connected = False
			print("Disconnected.")

	def discoverMaze(self):
		# explorer = Explore(self.robot)
		# explorer.run()
		laby = self.robot.discoverMaze() #DONE !!
		print("Discovery ended..")
		laby.printLaby()
		self.send(laby)



if __name__ == '__main__':
	
	#création du robot et connection au serveur (config dans config.py)
	rb = RobotClient()

	#run
	rb.run()

