## RobotClient

Dossier contenant les fichiers permettant la connexion au serveur (sur la raspberry)

Configuration de la connexion dans le fichier config/config.py.

Le main (dans le fichier client.py) créé un robotClient, se connecte au serveur, et créé un lien de communication.