# !/usr/bin/env python3
# -*- coding: utf-8 -*-


import ev3dev
import ev3dev.ev3 as ev3

import math
import time
import random
import threading

from laby import *

# gestion des couleurs
# 0 == None
# 1 == Black
# 5 == Red
# 6 == White
dicoCouleur = {
    "O":[1, 6],
    "E":[6, 1],
    "N":[1, 5],
    "S":[5, 1]
}

class Robot:
    def __init__(self, posX=0, posY=0, labX=5, labY=5, orientation="S"):
        self.client = None
        # config
        # changer selon l'état de fatigue du robot
        # -0.2 au ratio == 10degrès de moins quand il tourne
        self.ratio_moteur_roue = 0.13
        self.turn_speed        = 150
        self.line_speed        = 400
        
        self.posX = posX
        self.posY = posY
        self.orientation = orientation

        self.labX = labX
        self.labY = labY

        self.laby = None

        self.teteTourneeAGauche = False
        self.teteTourneeADroite = False
        
        self.tile_size         = 22 # en cm
        self.ROT_PER_TILE      = 1027

        self.pas = 0.0125
        # variables
        self.ratio_taco_cm  = 0.24
        self.btn            = ev3.Button()
        self.valueAdjusting = 40

        # color needed to turn right
        self.color_for_turn = 0

        # motors
        self.left_motor  = ev3.Motor('outD')
        self.right_motor = ev3.Motor('outA')
        self.head_motor  = ev3.Motor('outB')

        # self.head_motor.reset()
        self.ABSOLUTEPOS_HEAD_FRONT = 0

        self.motors = [self.left_motor, self.left_motor]
        for motor in self.motors:
            motor.stop_action = 'hold'

        # sensors
        self.color_sensor = ev3dev.core.ColorSensor()
        self.us_sensor    = ev3dev.core.UltrasonicSensor()
        self.gyro_sensor  = ev3dev.core.GyroSensor()
        self.sensors      = [self.color_sensor, self.us_sensor, self.gyro_sensor]

        self.gyro_sensor.mode='GYRO-ANG'
        self.us_sensor.mode = 'US-DIST-CM'

        # if orientation is None:
        #     self.scan_color_near()

        # gestion des couleurs
        # 0 == None
        # 1 == Black
        # 5 == Red
        # 6 == White
        self.right_color = None
        self.left_color  = None
        leftAndRightColor = dicoCouleur[self.orientation]
        if self.orientation == "O" or self.orientation == "N":
            self.current_color = leftAndRightColor[1]
            self.left_color    = leftAndRightColor[0]
        else:
            self.current_color = leftAndRightColor[0]
            self.right_color   = leftAndRightColor[1]
        self.color_sensor.mode = 'COL-COLOR'

    # Quand le programme termine, relacher la tension sur les moteurs
    def __del__(self):
        for motor in self.motors:
            motor.reset()
    
    def getGyro_sensorValue(self):
        return self.gyro_sensor.value()
    
    def setLaby(self, newLaby):
        self.laby = newLaby

    def getLaby(self):
        return self.laby

    def panel_dist_1(self,distance):
        return distance>=0 and distance<15

    def panel_dist_2(self,distance):
        return distance>=15 and distance<41

    def panel_dist_3(self,distance):
        return distance>=41 and distance<75

    def panel_dist_4(self,distance):
        return distance>=75 and distance<=103

    # TODO seems impossible to be as precise as we want from here
    def panel_dist_5(self,distance):
        return distance>103
    
    # TODO seems impossible to be as precise as we want from here
    # def panel_dist_6(self,distance):
    #     return distance>0 and distance<10

    # FONCTION qui renvoie la distance qui s�pare le robot du panneau le plus proche
    def panel_dist(self,distance):
        if self.panel_dist_1(distance):
            return 1
        elif self.panel_dist_2(distance):
            return 2
        elif self.panel_dist_3(distance):
            return 3
        elif self.panel_dist_4(distance):
            return 4
        # elif self.panel_dist_5(distance):
        #     return 5
        # elif self.panel_dist_6(distance):
        #     return 6
        else:
            return 0


    def tournerForever(self, goRightOrLeft=1):
        count_per_rot = self.right_motor.count_per_rot

        self.right_motor.run_forever(speed_sp=80*goRightOrLeft)
        self.left_motor.run_forever(speed_sp=-80*goRightOrLeft)
        self.left_motor.wait_until_not_moving(10000)

    def tourner(self, nb_cases=1):
        count_per_rot = self.right_motor.count_per_rot

        self.right_motor.run_to_rel_pos(position_sp=nb_cases * self.tile_size * self.ratio_taco_cm * count_per_rot * self.ratio_moteur_roue,
                                        speed_sp=self.turn_speed)
        self.left_motor.run_to_rel_pos(position_sp=-nb_cases * self.tile_size * self.ratio_taco_cm * count_per_rot * self.ratio_moteur_roue,
                                    speed_sp=self.turn_speed)
        self.left_motor.wait_until_not_moving(10000)


    def tournerAvecPas(self, goRightOrLeft):
        while(True):
            self.tourner(-self.pas*goRightOrLeft)
            currentColorDetected = self.color_sensor.value()
            if currentColorDetected is self.current_color:
                break
            if(self.btn.any()):
                self.stop()
                break
        self.stop()
    # goRightOrLeft = 1 : tourne vers la gauche
    # goRightOrLeft = -1 : tourne vers la droite
    def tournerCouleur(self, goRightOrLeft=1):
        pas = 0.025*goRightOrLeft * 0.5
        currentColorDetected = self.color_sensor.value()
        if self.orientation == "O" :
            if goRightOrLeft == -1:
                self.orientation = "S"
                valDico = dicoCouleur[self.orientation]
                self.current_color = valDico[0]
                self.right_color   = valDico[1]
                self.left_color    = None
            if goRightOrLeft == 1:
                self.orientation = "N"
                valDico = dicoCouleur[self.orientation]
                self.current_color = valDico[1]
                self.left_color    = valDico[0]
                self.right_color   = None
        elif self.orientation == "S":
            if goRightOrLeft == -1:
                self.orientation = "E"
                valDico = dicoCouleur[self.orientation]
                self.current_color = valDico[0]
                self.right_color   = valDico[1]
                self.left_color    = None
            if goRightOrLeft == 1:
                self.orientation = "O"
                valDico = dicoCouleur[self.orientation]
                self.current_color = valDico[1]
                self.left_color    = valDico[0]
                self.right_color   = None
        elif self.orientation == "E":
            if goRightOrLeft == -1:
                self.orientation = "N"
                valDico = dicoCouleur[self.orientation]
                self.current_color = valDico[1]
                self.left_color    = valDico[0]
                self.right_color   = None
            if goRightOrLeft == 1:
                self.orientation = "S"
                valDico = dicoCouleur[self.orientation]
                self.current_color = valDico[0]
                self.right_color   = valDico[1]
                self.left_color    = None
        else:
            if goRightOrLeft == -1:
                self.orientation = "O"
                valDico = dicoCouleur[self.orientation]
                self.current_color = valDico[1]
                self.left_color    = valDico[0]
                self.right_color   = None
            if goRightOrLeft == 1:
                self.orientation = "E"
                valDico = dicoCouleur[self.orientation]
                self.current_color = valDico[0]
                self.right_color   = valDico[1]
                self.left_color    = None
        currentColorDetected = self.color_sensor.value()
        # print("LOOKING FOR : "+str(self.current_color))

        self.tournerAvecPas(goRightOrLeft)

        self.stop()
        time.sleep(1)
        AngleDroit = 80
        gyroValue = abs(self.gyro_sensor.value())%AngleDroit
        pas = self.pas*goRightOrLeft
        # on ajuste avec le gyroscope
        if gyroValue < AngleDroit-5:
            while(abs(self.gyro_sensor.value())%AngleDroit <= AngleDroit):
                self.tourner(-self.pas*goRightOrLeft)
                currentColorDetected = self.color_sensor.value()
                if currentColorDetected is self.current_color:
                    break
                if(self.btn.any()):
                    self.stop()
                    break
            self.stop()
        elif gyroValue > AngleDroit+5:
            while(abs(self.gyro_sensor.value())%AngleDroit >= AngleDroit):
                self.tourner(self.pas*goRightOrLeft)
                currentColorDetected = self.color_sensor.value()
                if currentColorDetected is self.current_color:
                    break
                if(self.btn.any()):
                    self.stop()
                    break
            self.stop()

    def tournerTeteGauche(self):
        if not self.teteTourneeAGauche:
            self.teteTourneeAGauche = True
            self.teteTourneeADroite = False
            self.head_motor.wait_until_not_moving(1000)
            self.head_motor.run_timed(time_sp=2000, speed_sp=-self.line_speed)
            oldOrientation = self.orientation
            if oldOrientation == "N":
                self.orientation = "O"
            elif oldOrientation == "S":
                self.orientation = "E"
            elif oldOrientation == "E":
                self.orientation = "N"
            elif oldOrientation == "O":
                self.orientation = "S"
        self.head_motor.wait_until_not_moving(1000)

    def tournerTeteDroite(self):
        if not self.teteTourneeADroite:
            self.teteTourneeAGauche = False
            self.teteTourneeADroite = True
            self.head_motor.wait_until_not_moving(1000)
            self.head_motor.run_timed(time_sp=2000, speed_sp=self.line_speed)
            oldOrientation = self.orientation
            if oldOrientation == "N":
                self.orientation = "E"
            elif oldOrientation == "S":
                self.orientation = "O"
            elif oldOrientation == "O":
                self.orientation = "N"
            elif oldOrientation == "E":
                self.orientation = "S"
        self.head_motor.wait_until_not_moving(1000)

    def teteEnFace(self):
        self.head_motor.wait_until_not_moving(1000)
        self.head_motor.run_to_abs_pos(position_sp=self.ABSOLUTEPOS_HEAD_FRONT,speed_sp=100)
        oldOrientation = self.orientation
        if self.teteTourneeAGauche:
            self.teteTourneeAGauche = False
            if oldOrientation == "N":
                self.orientation = "E"
            elif oldOrientation == "S":
                self.orientation = "O"
            elif oldOrientation == "E":
                self.orientation = "S"
            elif oldOrientation == "O":
                self.orientation = "N"
        elif self.teteTourneeADroite:
            self.teteTourneeADroite = False
            if oldOrientation == "N":
                self.orientation = "O"
            elif oldOrientation == "S":
                self.orientation = "E"
            elif oldOrientation == "E":
                self.orientation = "N"
            elif oldOrientation == "O":
                self.orientation = "S"
        self.head_motor.wait_until_not_moving(1000)

    def regarder(self):
        self.us_sensor.mode = 'US-DIST-CM'
        Dleft = self.regarder_gauche()
        Dright = self.regarder_droite()
        return Dleft,Dright

    ################################################
    ################################################
    ###### PARTIE AYANT UN IMPACT SUR LE LABY ######
    ################################################
    ################################################

    def regarder_gauche_et_casser(self):
        self.us_sensor.mode = 'US-DIST-CM'
        self.tournerTeteGauche()
        time.sleep(1.5)
        Dleft = self.us_sensor.distance_centimeters
        nbMurs = self.panel_dist(Dleft)-1
        self.laby.multipleMurBreak(self.orientation, self.posX, self.posY, nbMurs)

        # on remet la tete a la bonne position dans TOUS les cas
        self.teteEnFace()

        return Dleft

    def regarder_droite_et_casser(self):
        self.us_sensor.mode = 'US-DIST-CM'
        self.tournerTeteDroite()
        time.sleep(1.5)
        Dright = self.us_sensor.distance_centimeters
        nbMurs = self.panel_dist(Dright)-1
        self.laby.multipleMurBreak(self.orientation, self.posX, self.posY, nbMurs)

        # on remet la tete a la bonne position dans TOUS les cas
        self.teteEnFace()

        return Dright

    def regarder_enface_et_casser(self):
        self.us_sensor.mode = 'US-DIST-CM'
        self.teteEnFace()
        time.sleep(1.5)
        Denface = self.us_sensor.distance_centimeters
        nbMurs = self.panel_dist(Denface)-1
        self.laby.multipleMurBreak(self.orientation, self.posX, self.posY, nbMurs)

        return Denface

    ####################################################
    ####################################################
    ####### FIN PARTIE AYANT UN IMPACT SUR LE LABY #####
    ####################################################
    ####################################################

    def regarder_gauche(self):
        self.us_sensor.mode = 'US-DIST-CM'
        # count_per_rot1 = self.head_motor.count_per_rot
        
        self.tournerTeteGauche()
        time.sleep(1.5)
        Dleft = self.us_sensor.distance_centimeters

        self.teteEnFace()
        return Dleft

    def regarder_droite(self):
        self.us_sensor.mode = 'US-DIST-CM'
        # count_per_rot1 = self.head_motor.count_per_rot

        self.tournerTeteDroite()
        time.sleep(1.5)
        Dright = self.us_sensor.distance_centimeters

        self.teteEnFace()
        return Dright


    def avancer(self):
        self.deplacer(1)

    def avancer_infini(self):
        self.right_motor.run_forever(speed_sp=self.line_speed)
        self.left_motor.run_forever(speed_sp=self.line_speed)
    
    def reculer(self):
        self.deplacer(-1)

    def reculer_infini(self):
        self.right_motor.run_forever(speed_sp=-self.line_speed)
        self.left_motor.run_forever(speed_sp=-self.line_speed)

    # valeurs correctes pour rotation : -1 (sens horaire), 1 (sens trigo)
    # Vous pouvez aussi donner 2 ou -2 pour faire un demi-tour, ou 4 pour faire joyeusement un tour sur vous-même
    def deplacer(self, nbCases=1):
        count_per_rot = self.right_motor.count_per_rot
        count_per_rot2 = self.left_motor.count_per_rot
        self.right_motor.run_to_rel_pos(position_sp=self.tile_size*nbCases * count_per_rot * self.ratio_moteur_roue, speed_sp=self.line_speed)
        self.left_motor.run_to_rel_pos(position_sp=self.tile_size*nbCases * count_per_rot2 * self.ratio_moteur_roue, speed_sp=self.line_speed)

        self.left_motor.wait_until_not_moving(10000)

    def tournerDroite(self):
        self.tourner(-1)
    
    def tournerGauche(self):
        self.tourner(1)

    def stop(self):
        self.right_motor.stop(stop_action="hold")
        self.left_motor.stop(stop_action="hold")
    
    # check si le robot a un mur en face de lui
    def obstacleFront(self, distance=8.0):
        obstacleFrontAt = self.us_sensor.distance_centimeters
        if(obstacleFrontAt < distance):
            return True
        return False

    # check aussi si le robot est collé aux bords du laby
    def obstacleFrontAdvanced(self, distance=10.0):
        labyLongueur = self.laby.getLongueur()-1
        labyLargeur = self.laby.getLargeur()-1
        if self.orientation == "O" and self.posX == 0:
            # si le robot collé au mur de gauche
            return True
        elif self.orientation == "E" and self.posX == labyLargeur:
            # si le robot collé au mur de droite
            return True
        elif self.orientation == "N" and self.posY == 0:
            # si le robot collé au mur d'en haut
            return True
        elif self.orientation == "S" and self.posY == labyLongueur:
            # si le robot collé au mur d'en bas
            return True
        else:
            return self.obstacleFront()

    def all_motors_wait(self, cond, timeOut=None):
        if(cond):
            for motor in self.motors:
                motor.wait(cond,timeOut)

    def forward_until_obstacle(self):
        self.avancer_infini()
        while(True):
            if self.obstacleFront(8.0):
                break
        self.stop()

    def wait_while_running(self):
        for motor in self.motors:
            motor.wait_while('running')

    def motorsRunning(self):
        for motor in self.motors:
            if(motor.is_running):
                return True
        return False

    def forward_left_obstacle(self, maxObstacle=2):
        nbObstacle = maxObstacle
        while(nbObstacle > 0):
            while(not self.obstacleFront()):
                if(not self.motorsRunning()):
                    self.avancer_infini()
            nbObstacle -= 1
            self.tournerGauche()
        self.stop()

    def turnBack_obstacle(self, maxObstacle=2):
        nbObstacle = maxObstacle
        while(nbObstacle > 0):
            while(not self.obstacleFront(38.0)):
                if(not self.motorsRunning()):
                    self.avancer_infini()
            nbObstacle -= 1
            self.tourner(-2)
        self.stop()

    def wiggle(self):
        i = 0
        while(i < 10):
            self.tourner(0.2)
            self.tourner(-0.2)
            i += 1

    def adjust_left(self):
        self.left_motor.run_forever(speed_sp=self.line_speed-self.valueAdjusting)
        self.right_motor.run_forever(speed_sp=self.line_speed+self.valueAdjusting)
        time.sleep(0.001)

    def adjust_right(self):
        self.left_motor.run_forever(speed_sp=self.line_speed+self.valueAdjusting)
        self.right_motor.run_forever(speed_sp=self.line_speed-self.valueAdjusting)
        time.sleep(0.001)

    def wiggleIfOtherBot(self):
        self.forward_until_obstacle()
        self.us_sensor.mode = 'US-LISTEN'
        time.sleep(1.0)
        # print("self.us_sensor.other_sensor_present() == {}".format(self.us_sensor.other_sensor_present))
        if(self.us_sensor.other_sensor_present != 0):
            self.wiggle()
        # print("je STOP !")
        self.stop()
        self.us_sensor.mode = 'US-DIST-CM'

    def inferOrientation(self, black_on_left):
        if black_on_left and self.current_color == 6:
            self.orientation = "O"
            print("OUEST")
        elif black_on_left and self.current_color == 5:
            self.orientation = "N"
            print("NORD")
        elif not black_on_left and self.current_color == 6:
            self.orientation = "E"
            print("EST")
        elif not black_on_left and self.current_color == 5:
            self.orientation = "S"
            print("SUD")
        else:
            print("Error in inferOrientation line 366")
            print("Current color == {0} and black is left : {1}".format(self.current_color, black_on_left))

    def scan_color_near(self,goRightOrLeft=1):
        self.color_sensor.mode = 'COL-COLOR'
        pas = self.pas*goRightOrLeft
        i = 0
        oldcolor = 0
        list_color = []
        black_on_left = False
        while(len(list_color) < 2):
            currentColorDetected = self.color_sensor.value()
            if(currentColorDetected != oldcolor and (currentColorDetected==1 or currentColorDetected==6 or currentColorDetected==5)):
                list_color.append(currentColorDetected)
                oldcolor = currentColorDetected
            self.tourner(-pas)
            i += pas
            if(self.btn.any()):
                self.stop()
                return 0
        self.tourner(i)
        if(list_color[0] == 6 or list_color[0] == 5):
            self.current_color = list_color[0]
            self.right_color = list_color[1]
            self.left_color = None
            # print("Le noir est A DROITE!")
        else:
            self.current_color = list_color[1]
            self.left_color = list_color[0]
            self.right_color = None
            black_on_left = True
            # print("Le noir est A GAUCHE!")
        currentColorDetected = self.color_sensor.value()
        while(currentColorDetected is not self.current_color):
            self.tourner(-pas)
            currentColorDetected = self.color_sensor.value()
            if(self.btn.any()):
                self.stop()
                return
        # Ici on met a jour l'orientation du robot
        self.inferOrientation(black_on_left)

        
    def avanceNbCases(self,nbCases=1):
        labyLongueur = self.laby.getLongueur()-1
        labyLargeur = self.laby.getLargeur()-1

        self.teteEnFace()
        if self.orientation is None:
            self.scan_color_near()
        posDEPART = self.left_motor.position
        self.avancer_infini()
        for i in range(nbCases):
            isNotOnRightColor = False
            while(True):
                currentColorDetected = self.color_sensor.value()
                if(currentColorDetected != self.current_color):
                    if not isNotOnRightColor:
                        isNotOnRightColor = True
                        currentTime = time.time()
                    if self.left_color is not None and self.right_color is None:
                        if time.time() - currentTime == 0.75:
                            self.stop()
                            self.tournerCouleur()
                            self.avancer_infini()
                            continue
                        if currentColorDetected == self.current_color:
                            self.avancer_infini()
                        elif currentColorDetected == self.left_color:
                            self.adjust_right()
                        else:
                            self.adjust_left()
                    elif self.right_color is not None and self.left_color is None:
                        if time.time() - currentTime == 0.75:
                            self.stop()
                            self.tournerCouleur(-1)
                            self.avancer_infini()
                            continue
                        if currentColorDetected == self.current_color:
                            self.avancer_infini()
                        elif currentColorDetected == self.right_color:
                            self.adjust_left()
                        else:
                            self.adjust_right()
                else:
                    isNotOnRightColor = False
                    self.avancer_infini()
                if (self.left_motor.position-posDEPART) >= self.ROT_PER_TILE:
                # newTime = time.time() - currentTime
                # if newTime > 2.54:
                    break
                if self.obstacleFront(5.0):
                    break
                if self.btn.any():
                    self.stop()
                    return
            self.stop()
            self.newPos()
        # self.regarder_et_casser()
    
    def newPos(self):
        oldX = self.posX
        oldY = self.posY
        if self.orientation == "O":
            self.posX -= 1
        elif self.orientation == "E":
            self.posX += 1
        elif self.orientation == "S":
            self.posY += 1
        else:
            self.posY -= 1
        self.laby.setPosRobot(oldX, oldY, self.posX, self.posY)

    def stats_right_left(self):
        mean_right = 0
        mean_left = 0
        max_right = 0
        min_right = 500
        max_left = 0
        min_left = 500
        for i in range(10):
            left, right = self.regarder()
            if left > max_left:
                max_left = left
            if left < min_left:
                min_left = left
            if right > max_right:
                max_right = right
            if right < min_right:
                min_right = right
            mean_right += right
            mean_left += left
            print('distance left :' , left)
            print('distance right :' , right)    
        print('distance MEAN left :' , mean_left/10)
        print('distance MEAN right :' , mean_right/10)
        print('MIN left :' , min_left)
        print('MAX left :' , max_left)
        print('MIN right :' , min_right)
        print('MAX right :' , max_right)

    def regarder_et_casser(self):
        labyLongueur = self.laby.getLongueur()-1
        labyLargeur = self.laby.getLargeur()-1
        if self.orientation == "O":
            # si je ne suis suis PAS sur la ligne du bas
            if self.posY != labyLongueur:
                left = self.regarder_gauche_et_casser()
            # si je ne suis PAS sur la ligne du haut
            if self.posY != 0:
                right = self.regarder_droite_et_casser()
            # si je ne suis pas collé au mur de gauche
            if self.posX != 0:
                front = self.regarder_enface_et_casser()
        elif self.orientation == "E":
            # si je ne suis suis PAS sur la ligne du bas
            if self.posY != labyLongueur:
                right = self.regarder_droite_et_casser()
            # si je ne suis PAS sur la ligne du haut
            if self.posY != 0:
                left = self.regarder_gauche_et_casser()
            # si je ne suis pas collé au mur de droite
            if self.posX != labyLargeur:
                front = self.regarder_enface_et_casser()
        elif self.orientation == "N":
            # si je ne suis suis PAS sur la ligne de gauche
            if self.posX != 0:
                left = self.regarder_gauche_et_casser()
            # si je ne suis PAS sur la ligne de droite
            if self.posX != labyLargeur:
                right = self.regarder_droite_et_casser()
            # si je ne suis pas collé au mur d'en haut
            if self.posY != 0:
                front = self.regarder_enface_et_casser()
        elif self.orientation == "S":
            # si je ne suis suis PAS sur la ligne de gauche
            if self.posX != 0:
                right = self.regarder_droite_et_casser()
            # si je ne suis PAS sur la ligne de droite
            if self.posX != labyLargeur:
                left = self.regarder_gauche_et_casser()
            # si je ne suis pas collé au mur d'en bas
            if self.posY != labyLongueur:
                front = self.regarder_enface_et_casser()
        # nbPanelL = self.panel_dist(left)
        # nbPanelR = self.panel_dist(right)
        # nbPanelFront = self.panel_dist(front)

        # DEBUG #
        # print('distance left CM :' , left)
        # print('distance right CM :' , right)
        # print('distance front CM :' , front)
        # print('nbPanel left :' , nbPanelL)
        # print('nbPanel right :' , nbPanelR)
        # print('nbPanel front :' , nbPanelFront)
        # DEBUG #

    """
    PARTIE CLIENT / SERVEUR
    """
    def discoverMaze(self, nbIteration=16):
        self.laby = Laby(self.labX,self.labY,self.posX,self.posY)
        tourneUneFois = False
        tourneDeuxFois = False
        for i in range(nbIteration):
            self.regarder_et_casser()
            if self.obstacleFrontAdvanced():
                if (not tourneUneFois) and (not tourneDeuxFois):
                    print("Je tourne UNE fois")
                    if self.posX == 0 and self.orientation == "S":
                        self.tournerCouleur(-1)
                    elif self.posX == 0 and self.orientation == "N":
                        self.tournerCouleur()
                    elif self.posY == 0 and self.orientation == "E":
                        self.tournerCouleur()
                    elif self.posY == 0 and self.orientation == "O":
                        self.tournerCouleur(-1)
                    else:
                        self.tournerCouleur()
                    tourneUneFois = True
                    continue
                elif tourneUneFois and (not tourneDeuxFois):
                    print("Je tourne DEUX fois")
                    self.tournerCouleur()
                    tourneDeuxFois = True
                    continue
                else:
                    self.tournerCouleur(-1)
                    continue
            else:
                tourneUneFois = False
            self.avanceNbCases()
            # print("Itération numéro : "+str(i))
            laby = self.getLaby()
            laby.printLaby()
            time.sleep(1)
        print("Scan done !")
        return self.laby
        
    def setClient(self, client):
        self.client = client

    def avancerNord(self):
        print("avancerNord")
        if self.orientation != "N":
            if self.orientation == "O":
                self.tournerCouleur()
            if self.orientation == "E":
                self.tournerCouleur(-1)
            if self.orientation == "S":
                self.tournerCouleur()
                self.tournerCouleur()
        self.forward_until_obstacle()

    def avancerSud(self):
        print("avancerSud")
        if self.orientation != "S":
            if self.orientation == "O":
                self.tournerCouleur(-1)
            if self.orientation == "E":
                self.tournerCouleur()
            if self.orientation == "N":
                self.tournerCouleur()
                self.tournerCouleur()
        self.forward_until_obstacle()

    def avancerEst(self):
        print("avancerEst")
        if self.orientation != "E":
            if self.orientation == "O":
                self.tournerCouleur()
                self.tournerCouleur()
            if self.orientation == "N":
                self.tournerCouleur()
            if self.orientation == "S":
                self.tournerCouleur(-1)
        self.forward_until_obstacle()

    def avancerOuest(self):
        print("avancerOuest")
        if self.orientation != "O":
            if self.orientation == "E":
                self.tournerCouleur()
                self.tournerCouleur()
            if self.orientation == "N":
                self.tournerCouleur(-1)
            if self.orientation == "S":
                self.tournerCouleur()
        self.forward_until_obstacle()

    def setInitialPos(self, X, Y, orientation) :
        self.posX = int(X)
        self.posY = int(Y)
        self.orientation = orientation
        print("Received initial Pos : " + str(X) + ", " + str(Y) + ", orientation = " + str(orientation))

    def setLabSize(self, labX, labY) :
        self.labX = int(labX)
        self.labY = int(labY)
        print("Received lab size : x = " + str(labX) + ", y = " + str(labY))
