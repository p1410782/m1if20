#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from server import ServerIA
from maze_solver import *

import sys
sys.path.append("..")
from laby import *

class IA:
	def __init__(self, nbOfRobots):
		print("init_ia")
		self.nbRobot = nbOfRobots
		self.end = False
		self.sia = None

		self.laby = None #Labyrinthe découvert par le premier robot

		self.labX = 0
		self.labY = 0

		self.robotOK = False #True lorsque le robot a fini son coup

	def setServer(self, server):
		self.sia = server

	def run(self):
		""" Boucle principale (sur l'ordinateur)
			Permet de contrôler les robots, lancer la découverte, lancer le jeu, quitter
		"""
		while not self.end:
			cmd = input("wait commande, 0=quit, 1=discover ou 2=play the game\n")

			if cmd == "1":
				self.discoverTheMaze()
			elif cmd == "2":
				# Launch the maze solver to get the solution
				# Then communicate with the robots to move them
				self.solveTheMaze()
			elif cmd == "0":
				print("IA:: end = true")
				self.end = True

	def discoverTheMaze(self) :
		""" Lance la découverte du labyrinthe par le premier robot
			Récupère et sauvegarde le labyrinthe
		"""
		print("IA:: lancement de la découverte du maze")
		print("Taille du maze : ")
		self.labX = int(input("Taille en X = "))
		self.labY = int(input("Taille en Y = "))

		#On envoie la taille du labyrinthe aux clients
		self.sendMessage(-1, (self.labX,self.labY)) #Sous forme de tuple

		#Input la position initiale du robot
		initPosRobot = self.inputPosRobot(0)

		#On envoie au robot 0 sa position
		self.sendMessage(0, initPosRobot)

		#Attente
		time.sleep(.5)

		self.sendMessage(0, "DISCOVER") #Le premier robot fait la découverte
		#Attente du labyrinthe envoyé par le robot
		print("Attente de la fin de la découverte...")
		timeout = 0
		maxTimeOutSec = 1500
		while self.laby == None :
			time.sleep(.1) #TODO Add timeout
			timeout += 0.1
			if timeout > maxTimeOutSec :
				print("Timeout " + str(maxTimeOutSec) + "s découverte du maze, stop waiting.")
				return
		print("Labyrinthe reçu : ")
		self.laby.printLaby()

	def solveTheMaze(self) :
		""" Après avoir récupéré le labyrinthe, on résoud le maze
			Résoudre = atteindre une case voulue, avec des positions initiales des robots
			Avec un robot voulu
		"""

		# if self.labX == 0 or self.labY == 0 :
		# 	print("Il faut d'abord faire la découverte du maze...")
		# 	return

		if self.nbRobot != 3 :
			print("Ne fonctionne qu'avec 3 robots....")
			return

		xVoulu = input("Entrez la position X voulue : ")
		yVoulu = input("Entrez la position Y voulue : ")
		caseVoulue = (int(xVoulu), int(yVoulu))

		#Numéro du robot qui doit se rendre à la case voulue
		robotSolver = input("Entrez l'indice du robot qui doit se rendre à la case [" + xVoulu + "," + yVoulu + "] : (entre 0 et " + str(self.nbRobot - 1) + ") : ")

		#Positions initiales des robots :
		initPos = []
		for i in range(self.nbRobot) :
			pos = self.inputPosRobot(i)
			initPos.append(pos)
			#Envoi de la position initiale au robot
			self.sendMessage(i, pos)


		#TODO création du mazeSolver
		#3 robots obligatoirement.....
		#Pour le mazeSolver = pas d'orientation
		
		#maze = Maze(self.labX, self.labY, initPos[0][:2], initPos[1][:2], initPos[2][:2], caseVoulue)

		# #TODO Créer les murs à partir de laby !!
		#maze = self.createMazeWallFromLaby(maze)

		maze = Maze(3, 5, [0, 0], [1, 0], [0, 1], [1, 3])
		maze.place_wall([0, 3], [1, 3])
		maze.place_wall([1, 3], [2, 3])


		# #Le maze est créé, on le solve
		solution, nbCoups = find_solutions_brute_force(maze)

		print("Solution trouvée en " + str(nbCoups) + " coups.")

		self.envoieOrdresAuxRobots(solution)

		#TEST :

		#solution, nbCoup = maze1()
		#self.envoieOrdresAuxRobots(solution)


	def envoieOrdresAuxRobots(self, solution) :

		# Solution : 
		# [ [1, 'S'], [2, 'O'] ]...
		#En secondes, tps d'attente message ok
		maxTimeout = 200
		for robotNb, orientation in solution :
			print("Envoi du message " + str(orientation) + " au robot " + str(robotNb))

			self.sendMessage(robotNb, orientation)

			#Attente du message OK du robot
			print("Attente du message OK du robot " + str(robotNb) + "....")
			to = 0
			while True:
				if self.robotOK :
					break #Le robot a fini son coup
				time.sleep(.1)
				to += 0.1
				if to > maxTimeout:
					print("Timeout robot.... Bug? On s'arrête!!")
					return
			print("Le robot a fini son coup ! On passe au coup suivant...")
			self.robotOK = False

		print("C'est fini !!")


	def createMazeWallFromLaby(self, maze) :
		matriceWalls = self.laby.getMatriceMur()
		for i in matriceWalls:
			if matriceWalls[i] == 1:
				x1,y1,x2,y2 = self.laby.findAdjacentCases(i)
				maze.place_wall([x1, y1], [x2, y2])

		#Create wall like this : maze.place_wall([0,1], [0,2])
		# signifie qu'on a un mur entre 0,1 et 0,2 (mur vertical..)


		return maze

	def inputPosRobot(self, robotNb) :
		x = input("X initial du robot numéro " + str(robotNb) + " : ")
		y = input("Y initial du robot numéro " + str(robotNb) + " : ")
		orientation = input("Orientation (E, S, O, N) : ")

		return [int(x),int(y),orientation]

	# Messages possibles : 
	# DISCOVER = start discovery
	# START = start the game
	# Si clientNumber == -1, envoi à tous les robots
	# Sinon envoi au clientNumber
	def sendMessage(self, clientNumber, msg):
		if self.sia is not None:
			if clientNumber == -1 :
				#Send message to all clients
				for i in range(self.nbRobot) :
					self.sia.sendMsgToClient(i, msg)
			else :
				self.sia.sendMsgToClient(clientNumber, msg)

	def processMessage(self, robotNumber, msg):
		""" Executée par les threads d'écoute aux clients (classe server)
		"""
		if isinstance(msg, Laby) :
			self.laby = msg #On sauvegarde le labyrinthe
		elif isinstance(msg, str) :
			print("Received " + msg)
			if msg == "OK" :
				self.robotOK = True

	def over(self):
		return self.end