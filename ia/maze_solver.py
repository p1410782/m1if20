import itertools
import time
from copy import deepcopy


def opposite_direction(direction):
    if direction == 'N':
        return 'S'
    elif direction == 'S':
        return 'N'
    elif direction == 'E':
        return 'W'
    elif direction == 'W':
        return 'E'


class Maze:
    def __init__(self, width, height, robot1_position, robot2_position, robot3_position, destination):
        self.width = width
        self.height = height
        self.maze = [[0 for x in range(self.height)] for y in range(self.width)]

        self.maze[robot1_position[0]][robot1_position[1]] = 'A'
        self.maze[robot2_position[0]][robot2_position[1]] = 'B'
        self.maze[robot3_position[0]][robot3_position[1]] = 'C'

        self.maze[destination[0]][destination[1]] = "x"

        self.robot_positions = [robot1_position, robot2_position, robot3_position]
        self.destination = destination

        self.walls_columns = [[0 for x in range(self.height)] for y in range(self.width)]
        for i in range(width):
            for j in range(height):
                self.walls_columns[i][j] = '  '

        self.walls_rows = [[0 for x in range(self.height)] for y in range(self.width)]
        for i in range(width):
            for j in range(height):
                self.walls_rows[i][j] = '  '

        self.current_x = 0
        self.current_y = 0
        self.fake_robot = destination

    def print_maze(self):
        for j in range(self.height):
            for i in range(self.width):
                if i is not self.width - 1:
                    print(self.maze[i][j], self.walls_columns[i][j], end='')
                else:
                    print(self.maze[i][j])

            for k in range(self.width):
                if k is not self.width - 1:
                    print(self.walls_rows[k][j], ' ', end='')
                else:
                    print(self.walls_rows[k][j])

    # wall must go from left to right or from top to bottom
    def place_wall(self, index_from, index_to):
        if index_from[0] != index_to[0]:
            assert abs(index_from[0] - index_to[0]) <= 1
        if index_from[1] != index_to[1]:
            assert abs(index_from[1] - index_to[1]) <= 1

        if index_from[0] == index_to[0]:
            self.walls_rows[index_from[0]][index_from[1]] = "- "
        if index_from[1] == index_to[1]:
            self.walls_columns[index_from[0]][index_from[1]] = "| "

    def can_go(self, index_from, index_to):
        wall = True
        if index_from[0] == index_to[0]:
            if index_to[1] > self.height - 1 or index_to[1] < 0:
                wall = False
            else:
                if index_from[1] < index_to[1]:
                    wall = self.walls_rows[index_from[0]][index_from[1]] == "  "
                else:
                    wall = self.walls_rows[index_from[0]][index_from[1] - 1] == "  "
        if index_from[1] == index_to[1]:
            if index_to[0] > self.width - 1 or index_to[0] < 0:
                wall = False
            else:
                if index_from[0] < index_to[0]:
                    wall = self.walls_columns[index_from[0]][index_from[1]] == "  "
                else:
                    wall = self.walls_columns[index_from[0] - 1][index_from[1]] == "  "

        # on check qu'il n'y ait pas de robot à la position voulue
        if index_to[0] < self.width and index_to[1] < self.height:
            if self.maze[index_to[0]][index_to[1]] == 'A' \
                    or self.maze[index_to[0]][index_to[1]] == 'B' \
                    or self.maze[index_to[0]][index_to[1]] == 'C':
                wall = False
        return wall

    def move_robot(self, index, direction):
        letter = 'A'
        if index == 1:
            letter = 'B'
        if index == 2:
            letter = 'C'

        self.current_x = self.robot_positions[index][0]
        self.current_y = self.robot_positions[index][1]
        if direction == 'E':
            while self.can_go([self.current_x, self.current_y], [self.current_x + 1, self.current_y]):
                self.maze[self.current_x][self.current_y] = 0
                self.maze[self.current_x + 1][self.current_y] = letter
                self.robot_positions[index][0] += 1
                self.current_x = self.robot_positions[index][0]
        if direction == 'S':
            while self.can_go([self.current_x, self.current_y], [self.current_x, self.current_y + 1]):
                self.maze[self.current_x][self.current_y] = 0
                self.maze[self.current_x][self.current_y + 1] = letter
                self.robot_positions[index][1] += 1
                self.current_y = self.robot_positions[index][1]
        if direction == 'W':
            while self.can_go([self.current_x, self.current_y], [self.current_x - 1, self.current_y]):
                self.maze[self.current_x][self.current_y] = 0
                self.maze[self.current_x - 1][self.current_y] = letter
                self.robot_positions[index][0] -= 1
                self.current_x = self.robot_positions[index][0]
        if direction == 'N':
            while self.can_go([self.current_x, self.current_y], [self.current_x, self.current_y - 1]):
                self.maze[self.current_x][self.current_y] = 0
                self.maze[self.current_x][self.current_y - 1] = letter
                self.robot_positions[index][1] -= 1
                self.current_y = self.robot_positions[index][1]

    def move(self, index, direction):

        if direction == "N":
            #print("Moving", index, "to north")
            self.move_robot(index, 'N')
            #self.print_maze()

        if direction == "S":
            #print("Moving", index, "to south")
            self.move_robot(index, 'S')
            #self.print_maze()

        if direction == "W":
            #print("Moving", index, "to west")
            self.move_robot(index, 'W')
            #self.print_maze()

        if direction == "E":
            #print("Moving", index, "to east")
            self.move_robot(index, 'E')
            #self.print_maze()

    def move_fake_robot(self, position, direction):

        self.fake_robot = position.copy()
        self.current_x = self.fake_robot[0]
        self.current_y = self.fake_robot[1]
        if direction == 'E':
            while self.can_go([self.current_x, self.current_y], [self.current_x + 1, self.current_y]):
                self.fake_robot[0] += 1
                self.current_x = self.fake_robot[0]
        if direction == 'S':
            while self.can_go([self.current_x, self.current_y], [self.current_x, self.current_y + 1]):
                self.fake_robot[1] += 1
                self.current_y = self.fake_robot[1]
        if direction == 'W':
            while self.can_go([self.current_x, self.current_y], [self.current_x - 1, self.current_y]):
                self.fake_robot[0] -= 1
                self.current_x = self.fake_robot[0]
        if direction == 'N':
            while self.can_go([self.current_x, self.current_y], [self.current_x, self.current_y - 1]):
                self.fake_robot[1] -= 1
                self.current_y = self.fake_robot[1]
        return self.fake_robot.copy()

    def find_possible_indexes(self, position):
        results = []
        x = position[0]
        y = position[1]
        if not self.can_go(position, [x, y - 1]):
            #print("can't go north")
            # on vérifie que bouger vers le sud fait bien bouger le robot
            if self.move_fake_robot(position, 'S') != self.destination:
                results.append(list(self.move_fake_robot(position, 'S').copy()))
        if not self.can_go(position, [x, y + 1]):
            #print("can't go south")
            # on vérifie que bouger vers le nord fait bien bouger le robot
            if self.move_fake_robot(position, 'N') != self.destination:
                results.append(list(self.move_fake_robot(position, 'N').copy()))
        if not self.can_go(position, [x + 1, y]):
            #print("can't go east")
            if self.move_fake_robot(position, 'W') != self.destination:
                results.append(list(self.move_fake_robot(position, 'W').copy()))
        if not self.can_go(position, [x - 1, y]):
            #print("can't go west")
            if self.move_fake_robot(position, 'E') != self.destination:
                results.append(list(self.move_fake_robot(position, 'E').copy()))
        return results

    def create_obstacles(self, position):
        moves_obstacles = []

        east_blocked = not self.can_go(position, [position[0] + 1, position[1]])
        west_blocked = not self.can_go(position, [position[0] - 1, position[1]])
        north_blocked = not self.can_go(position, [position[0], position[1] - 1])
        south_blocked = not self.can_go(position, [position[0], position[1] + 1])

        for i in range(3):
            old_position = self.robot_positions[i].copy()
            # block east
            if not east_blocked:
                self.move(i, 'E')
                if not self.can_go(position, [position[0] + 1, position[1]]):
                    moves_obstacles.append([i, 'E'])
                    east_blocked = True
                if old_position != self.robot_positions[i]:
                    self.move(i, 'W')

            # block north
            if not north_blocked:
                self.move(i, 'N')
                if not self.can_go(position, [position[0], position[1] - 1]):
                    moves_obstacles.append([i, 'N'])
                    north_blocked = True
                if old_position != self.robot_positions[i]:
                    self.move(i, 'S')

            # block west
            if not west_blocked:
                self.move(i, 'W')
                if not self.can_go(position, [position[0] - 1, position[1]]):
                    moves_obstacles.append([i, 'W'])
                    west_blocked = True
                if old_position != self.robot_positions[i]:
                    self.move(i, 'E')

            # block south
            if not south_blocked:
                self.move(i, 'S')
                if not self.can_go(position, [position[0], position[1] + 1]):
                    moves_obstacles.append([i, 'S'])
                    south_blocked = True
                if old_position != self.robot_positions[i]:
                    self.move(i, 'N')

        return moves_obstacles

    def can_a_robot_go_to(self, position):
        can_go = False
        for i in range(3):
            old_position = self.robot_positions[i].copy()
            # try east
            self.move(i, 'E')
            if self.robot_positions[i] == position:
                can_go = True
            if old_position != self.robot_positions[i]:
                self.move(i, 'W')

            # try north
            self.move(i, 'N')
            if self.robot_positions[i] == position:
                can_go = True
            if old_position != self.robot_positions[i]:
                self.move(i, 'S')

            # try south
            self.move(i, 'S')
            if self.robot_positions[i] == position:
                can_go = True
            if old_position != self.robot_positions[i]:
                self.move(i, 'N')
        return can_go

    def can_the_robot_go_to(self, index, position):
        can_go = False

        old_position = self.robot_positions[index].copy()
        # try east
        self.move(index, 'E')
        if self.robot_positions[index] == position:
            can_go = True
        if old_position != self.robot_positions[index]:
            self.move(index, 'W')

        # try north
        self.move(index, 'N')
        if self.robot_positions[index] == position:
            can_go = True
        if old_position != self.robot_positions[index]:
            self.move(index, 'S')

        # try south
        self.move(index, 'S')
        if self.robot_positions[index] == position:
            can_go = True
        if old_position != self.robot_positions[index]:
            self.move(index, 'N')
        return can_go

    def get_move_to_go_to(self, position):

        assert self.can_a_robot_go_to(position)

        for i in range(3):
            old_position = self.robot_positions[i].copy()
            # try east
            self.move(i, 'E')
            if self.robot_positions[i] == position:
                if old_position != self.robot_positions[i]:
                    self.move(i, 'W')
                return [i, 'E']
            if old_position != self.robot_positions[i]:
                self.move(i, 'W')

            # try north
            self.move(i, 'N')
            if self.robot_positions[i] == position:
                if old_position != self.robot_positions[i]:
                    self.move(i, 'S')
                return [i, 'N']
            if old_position != self.robot_positions[i]:
                self.move(i, 'S')
            # try south
            self.move(i, 'S')
            if self.robot_positions[i] == position:
                if old_position != self.robot_positions[i]:
                    self.move(i, 'N')
                return [i, 'S']
            if old_position != self.robot_positions[i]:
                self.move(i, 'N')

    def get_the_move_to_go_to(self, index, position):

        assert self.can_the_robot_go_to(index, position)

        old_position = self.robot_positions[index].copy()
        # try east
        self.move(i, 'E')
        if self.robot_positions[index] == position:
            if old_position != self.robot_positions[index]:
                self.move(i, 'W')
            return [index, 'E']
        if old_position != self.robot_positions[index]:
            self.move(i, 'W')

        # try north
        self.move(i, 'N')
        if self.robot_positions[index] == position:
            if old_position != self.robot_positions[index]:
                self.move(i, 'S')
            return [index, 'N']
        if old_position != self.robot_positions[index]:
            self.move(i, 'S')
        # try south
        self.move(i, 'S')
        if self.robot_positions[index] == position:
            if old_position != self.robot_positions[index]:
                self.move(i, 'N')
            return [index, 'S']
        if old_position != self.robot_positions[index]:
            self.move(i, 'N')

    def position_found(self):
        # on vérifie que le robot A est à la bonne position
        return self.robot_positions[0] == self.destination

    def copy(self):
        maze_copy = Maze(self.width,
                         self.height,
                         self.robot_positions[0].copy(),
                         self.robot_positions[1].copy(),
                         self.robot_positions[2].copy(),
                         self.destination.copy())
        maze_copy.walls_rows = self.walls_rows.copy()
        maze_copy.walls_columns = self.walls_columns.copy()
        return maze_copy

    def get_moves_that_has_no_effect(self):
        moves_with_no_effect = []
        for i in range(len(self.robot_positions)):
            [x, y] = self.robot_positions[i].copy()
            if not self.can_go([x, y], [x + 1, y]):
                moves_with_no_effect.append([i, 'E'])
            if not self.can_go([x, y], [x - 1, y]):
                moves_with_no_effect.append([i, 'W'])
            if not self.can_go([x, y], [x, y - 1]):
                moves_with_no_effect.append([i, 'N'])
            if not self.can_go([x, y], [x, y + 1]):
                moves_with_no_effect.append([i, 'S'])
        return moves_with_no_effect

    def contains_three_following_moves_with_no_effect(self, moves_list):
        position0 = self.robot_positions[0].copy()
        position1 = self.robot_positions[1].copy()
        position2 = self.robot_positions[2].copy()

        for move in moves_list:
            self.move(move[0], move[1])

        position0_after = self.robot_positions[0].copy()
        position1_after = self.robot_positions[1].copy()
        position2_after = self.robot_positions[2].copy()

        for move in reversed(moves_list):
            self.move(move[0], move[1])

        if position0_after == position0 \
            and position1_after == position1 \
                and position2_after == position2:
            return True

        return False


# ne marche pas génériquement
def find_solutions(maze):
    solutions = []

    first_possible_indexes = maze.find_possible_indexes(maze.destination)

    moves = []
    for i in range(len(first_possible_indexes)):
        while not maze.can_a_robot_go_to(first_possible_indexes[i]):
            moves_to_create_obstacles = maze.create_obstacles(first_possible_indexes[i])
            moves.append(moves_to_create_obstacles[0])
            maze.move(moves_to_create_obstacles[0][0], moves_to_create_obstacles[0][1])

            second_possible_indexes = maze.find_possible_indexes(first_possible_indexes[i])

            for i in range(len(second_possible_indexes)):
                while not maze.can_a_robot_go_to(second_possible_indexes[i]):
                    a = 1
                moves.append(maze.get_move_to_go_to(second_possible_indexes[i]))
                maze.move(moves[1][0], moves[1][1])
                break

            moves.append(maze.get_move_to_go_to(first_possible_indexes[i]))
            maze.move(moves[2][0], moves[2][1])
            break

        moves.append(maze.get_move_to_go_to(maze.destination))
        maze.move(moves[3][0], moves[3][1])
        solutions.append(moves)

    return solutions


def contains_two_following_opposite_moves(moves_list):
    for i in range(len(moves_list) - 1):
        if moves_list[i][0] == moves_list[i + 1][0] and moves_list[i][1] == opposite_direction(moves_list[i+1][1]):
            return True
    return False


def check_validity_solution(moves_list):
    if moves_list[len(moves_list) - 1][0] != 0:
        return False
    if contains_two_following_opposite_moves(moves_list):
        return False

    return True


# brute force
def find_solutions_brute_force(maze):
    # n = nb_coups
    n = 1
    found = False
    start_time = time.time()
    solution = []
    while n < 20 and not found:
        print("Trying", n, "moves")

        for i in itertools.product([
            [0, 'N'], [0, 'S'], [0, 'E'], [0, 'W'],
            [1, 'N'], [1, 'S'], [1, 'E'], [1, 'W'],
            [2, 'N'], [2, 'S'], [2, 'E'], [2, 'W']], repeat=n):

            if not check_validity_solution(list(i)):
                continue

            moves = list(i)
            maze_bis = maze.copy()

            #if len(moves) >= 3:
            #    if maze_bis.contains_three_following_moves_with_no_effect(moves):
            #        continue
            #maze_bis = maze.copy()

            no_effect = False

            happened_positions = []

            happened_positions.append(deepcopy(maze_bis.robot_positions))

            for move in moves:

                maze_bis.move(move[0], move[1])

                if maze_bis.robot_positions in happened_positions:
                    no_effect = True
                    break

                happened_positions.append(deepcopy(maze_bis.robot_positions))

            if no_effect:
                continue

            if maze_bis.position_found():
                print("Found with", n, "moves")
                print(moves)
                solution = moves.copy()
                maze_bis.print_maze()
                found = True
                elapsed_time = time.time() - start_time
                print("elapsed time : ", elapsed_time)
                break

        n += 1

    # Solution : 
    # [ [1, 'S'], [2, 'O'] ]...
    #n = nb de coups
    return solution, n


def maze1():
    maze = Maze(5, 5, [0, 0], [0, 4], [4, 4], [2, 2])
    maze.place_wall([3, 0], [3, 1])
    maze.place_wall([4, 0], [4, 1])
    maze.place_wall([2, 2], [2, 3])

    maze.print_maze()

    return find_solutions_brute_force(maze)


def maze2():
    maze = Maze(5, 3, [4, 0], [3, 0], [4, 1], [2, 1])
    maze.place_wall([1, 0], [1, 1])
    maze.place_wall([1, 1], [1, 2])

    maze.print_maze()

    find_solutions_brute_force(maze)


def maze3():
    maze = Maze(3, 5, [0, 0], [1, 0], [0, 1], [1, 3])
    maze.place_wall([0, 3], [1, 3])
    maze.place_wall([1, 3], [2, 3])

    maze.print_maze()

    find_solutions_brute_force(maze)


if __name__ == '__main__':
    print()
    print()
    print("Solving maze")
    maze1()
    print()
    print("Solving maze")
    print()
    maze3()
    print()
    print("Solving maze")
    print()
    maze2()



