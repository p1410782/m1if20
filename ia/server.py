#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import socket
import pickle
import sys
import threading
import select

from ia import *

sys.path.append("..")
from config.server_config import SERVER_ADDRESS, PORT
from robot_adam.laby import *

class ServerIA:
	def __init__(self):
		"""Création de l'ia, et socket serveur
		"""
		self.nbOfRobots = 3 #3 robots
		self.ia = IA(self.nbOfRobots)
		self.ia.setServer(self)

		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.socket.bind((SERVER_ADDRESS,PORT))
		print("Socket binded " + str(SERVER_ADDRESS))

		self.clients 		= [] # Contient des tuples (socket, address)
		self.threadsList 	= []
	
	def run(self):
		""" 
		"""
		self.socket.listen(1)
		print("Waiting for " + str(self.nbOfRobots) + " client..")
		#Wait for nbOfRobots clients
		while len(self.clients) != self.nbOfRobots :
		    # Wait for a connection
		    print('waiting for a connection')
		    clientConnection, clientAddress = self.socket.accept()
		    self.clients.append((clientConnection, clientAddress))

		print("Everyone is connected, starting...")
		self.start()
		print("Server exiting...")
		self.socket.close()

	def start(self):
		"""Lance un thread pour chaque clients qui écoute leurs messages reçus par l'IA
		   Puis lance l'IA
		"""
		print("Start thread listen client")
		for i,(clientConnection, clientAddress) in enumerate(self.clients):
			t = threading.Thread(target=self.listenClient, args=(i,clientConnection))
			self.threadsList.append(t)
			t.start()

		print("Start loop ia")
		self.ia.run()
		print("End ia, killing threads...")
		self.killThreads()

	def killThreads(self):
		for t in self.threadsList:
			#update ia.over??
			t.join()

	def listenClient(self, clientNumber, clientConnection):
		""" Ecoute les messages envoyés par le client clientNumber
			Utilise pickle pour reconstruire l'objet (ex labyrinthe, ou string..)
		"""
		clientConnection.setblocking(0)
		timeout_in_seconds = 1
		#end = False #True if client is disconnected
		while not self.ia.over():
			#Non blocking io
			ready = select.select([clientConnection], [], [], timeout_in_seconds)
			if ready[0]:
				#Data available for reading
				data = clientConnection.recv(1024)
				#Check if client is gone
				if data == b'':
					return
				if data:
					self.ia.processMessage(clientNumber, pickle.loads(data))
		print("Server::End listen client number = " + str(clientNumber))


	def sendMsgToClient(self, clientNumber, msg):
		"""
		"""	
		#print("Send " + msg)
		#self.clients[clientNumber][0].sendall(msg.encode())
		self.clients[clientNumber][0].sendall(pickle.dumps(msg))




if __name__ == '__main__':
	sia = ServerIA()
	sia.run()