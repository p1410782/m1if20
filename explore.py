from enum import IntEnum

"""

PAUL :

Il faudrait mettre explore dans ia/

Et au lieu d'appeler directement les méthodes de robot,
Utiliser des messages d'envoi (ex: envoyer le message "avancer" au robot)
Permettra de choisir quel robot on controle par l'ia

Si on laisse dans cette classe, on ne pourra pas avoir d'infos par l'ia sur
l'état de la découverte. SAuf si on demande au robot...
A voir.

"""

class Direction(IntEnum):
	RIGHT = 0
	UP = 1
	LEFT = 2
	DOWN = 3

	@classmethod
	def next(self, direction):
		return (direction+1) % 4

	@classmethod
	def opposite(self, direction):
		return (direction - 2) % 4

class Cell:
	""" Définit l'implementation d'une case de la grille
	"""
	def __init__(self, x, y):
		# Position relative au point de départ (0,0)
		self.x = x
		self.y = y
		#contient l'information d'acces aux cases voisines (Direction en indice)
		self.reachable = [None, None, None, None]

	def setReachable(self, direction):
		self.reachable[int(direction)] = True

	def setUnreachable(self, direction):
		self.reachable[int(direction)] = False

	def __eq__(self, other):
		""" Surcharge opérateur égalité
			Comparaison du x et y (pas du reachable) pour une autre cell, ou une liste [x,y]
		"""
		if isinstance(other, Cell) :
			return self.x == other.x and self.y == other.y
		elif isinstance(other, list) :
			return self.x == other[0] and self.y == other[1]
		else:
			print("Eq not implemented for Cell and " + str(type(other)))
			return False

class Explore:
	"""Wall follower (RIGHT) algorithm
	"""

	def initGrille(self):
		# # 2D grid
		# return [ [ Cell(i,j) for j in range(nb_col) ] for i in range(nb_row) ]
		# 1D grid with nb_col * nb_row cells
		#return [ Cell() for n in range(nb_col * nb_row) ]
		#init firstCell at position 0,0
		firstCell = Cell(0,0)
		#firstCell.setPosition(_position)
		return [firstCell]

	def __init__(self, robot, position=[0,0]) :
		self._robot              = robot
		#Toutes les cases découvertes seront définies
		#par rapport à la case de départ
		self._position           = position
		#Permet de connaitre le nombre max de cases
		self._nb_col             = 3
		self._nb_row             = 3
		#Au départ la grille ne contient qu'une case, la notre 
		self._indexCaseActuelle  = 0
		self._grille             = self.initGrille()
		self._direction          = Direction.UP
		self._detectionDistance  = 10.0       #A adapter
		self._completed          = False


	def run(self):
		print("Run discover")
		nbRotation = 0
		while not self.finished():
			print("Not finished")
			if self._robot.obstacleFront(self._detectionDistance):
				# La direction actuelle n'est pas reachable..
				print("Obstacle front")
				self.obstacleDevant()
				nbRotation += 1
			else:
				print("Pas d'obstacle")
				# Pas d'obstacle devant nous
				# Si on a deja exploré le chemin, on ne le prend pas (!= none) sauf si on a exploré toutes les directions de la case (rotation>=4)
				# Ce test empeche aussi de faire demi-tour sans a voir vu toutes les directions
				if self._grille[self._indexCaseActuelle].reachable[self._direction] == None or nbRotation >= 4:
					self.avancer()
					nbRotation = 0

					
	def obstacleDevant(self):
		""" Fonction appelée lorsqu'il y a un obstacle devant
			1. Mise à jour du reachable à false
			2. On tourne à gauche avec le robot
			3. Mise à jour de la direction
		"""
		self._grille[self._indexCaseActuelle].setUnreachable(self._direction)
		self._robot.tournerGauche()
		self._direction = Direction.next(self._direction)

	def avancer(self):
		""" Fonction appelée lorsqu'il n'y a pas d'obstacle devant
			et que l'on souhaite avancer
			1. Mise à jour du reachable de la case actuelle
			2. Avancer avec le robot + mise à jour de la position 
			3. Ajout de la case si elle est nouvelle
			4. Mise à jour du reachable de la nouvelle case
		"""
		self._grille[self._indexCaseActuelle].setReachable(self._direction)
		self._robot.avancer()

		#mise à jour de _position
		self.updatePosition()
		
		#création cellule si elle n'existe pas
		#mise à jour indexCaseActuelle
		self.updateIndexCaseActuelle()

		#La nouvelle case est atteignable à partir de la direction opposée
		self._grille[self._indexCaseActuelle].setReachable(Direction.opposite(self._direction))



	def updatePosition(self):
		"""Mise à jour de la position aprés chaque deplacement vers l'avant
		"""
		if (self._direction == Direction.RIGHT):
			self._position[0] += 1
		if (self._direction == Direction.UP):
			self._position[1] -= 1
		if (self._direction == Direction.LEFT):
			self._position[0] -= 1
		if (self._direction == Direction.DOWN):
			self._position[1] += 1

	def updateIndexCaseActuelle(self):
		""" Mise à jour de l'indice case actuelle
			Fonction appelée après mise à jour de la position.
			Création de la case si elle n'existe pas,
			Sinon récupération de son indice
		"""
		#_position a été mise à jour
		for index, cell in enumerate(self._grille):
			if cell == self._position :
				self._indexCaseActuelle = index
				return
		#On ne l'a pas trouvée, on l'ajoute
		self._indexCaseActuelle = len(self._grille)
		self._grille.append(Cell(self._position[0], self._position[1]))


	def finished(self):
		""" La découverte est terminée lorsqu'on a toutes les cases n'ont plus de direction à None
		"""
		for cell in self._grille :
			for way in cell.reachable :
				if way == None:
					return False
		return True
		#return len(self._grille) == self._nb_row * self._nb_col




